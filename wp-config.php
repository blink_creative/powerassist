<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'powerass_main');

/** MySQL database username */
define('DB_USER', 'powerass_main');

/** MySQL database password */
define('DB_PASSWORD', 'hKJa~)I>{{g%X8m');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'qVE<e=UUXi]QF|I&!-<N [bwWtjH(GcL;76yQ=SC[Slr1r_rH,8QUy[upmND3S0r');
define('SECURE_AUTH_KEY',  '0N-|h+y<;o>^Eu/5C-?DCxrVF9,odt=iZ:iQem5FK@gNI$D;]oCL!$3rI-b_Oh*m');
define('LOGGED_IN_KEY',    ';-!bZm9>HjX%C?B,l`xfZm71q)w2XB%_[d,2^)BC+u{Ph97[e*|D!jfRPktnQB7M');
define('NONCE_KEY',        'm~vSz5,kbNi:QXIjp_<tf=A@-]{oe*jngRn0<H16T-E^S]iWFIxUO~41ZZf.cD<g');
define('AUTH_SALT',        ';1!cUFT0Pg^ID|tsE9-B.9W#y@tLZZ/RK[@]kQfWPYa14?i%lHhDTM9TCi$dA^NF');
define('SECURE_AUTH_SALT', '-nIwE%x),I<$oNb}h7oV[:qk(-g:,?n<v:k;Ixd?I+>1T{<!a6IP.;S$M$F_J(<v');
define('LOGGED_IN_SALT',   'OR_?Q{D({@O7N@e5oG{TREdgFsHf>X%F4KLMBwP6RIKXpF#+*u+ a(]!~+3owM$A');
define('NONCE_SALT',       '0 I[}]iY6k6YByy(2Kr%R*S=L7t|-#(kb-hh59$?mrBl2^BuV|g vp+7t5-8P#_r');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
