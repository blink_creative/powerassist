<?php
/**
 * Template Name: Homepage
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header' ) ); ?>

	<?php if(is_array(get_field('activate_message') ) ){ ?>
	
	<!--//Alert-->

    <div id="urgent-message" class="modal hide fade">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Urgent message</h3>
		</div>
		<div class="modal-body">
			<?php echo get_field('urgent_message'); ?>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn btn-custom" data-dismiss="modal" aria-hidden="true">Close</a>
		</div>
    </div>
	
	<?php }; ?>

	<!--//Splash-->
	
	<div id="frame-splash">
		<div class="row-fluid">
			<div class="span8 hidden-phone">
				<div id="uc-container" class="uc-container">
					<div class="uc-initial-content">
						<span class="icon-eye"></span>
					</div>
					<div class="uc-final-content">
						<span class="icon-cancel"></span>
					</div>
				</div>
				<div id="slider" class="sl-slider-wrapper">
					<div class="sl-slider">
						<div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="-25" data-slice2-rotation="-25" data-slice1-scale="2" data-slice2-scale="2">
							<div class="sl-slide-inner">
								<div class="bg-img bg-img-1">
									<div class="left-container">
										<div class="splash-copy">
											<h2>Welcome to <span>Power Assist</span></h2>
											<blockquote></blockquote>
											<a href="/get-involved/" class="btn btn-custom btn-large">Learn more</a>									
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!--
					<nav id="nav-dots" class="nav-dots">
						<span class="nav-dot-current"></span>
						<span></span>
						<span></span>
					</nav>
					-->
				</div>
			</div>
			<div id="quick-search" class="span4">
				<div class="right-container">
					<div id="fixtures">
						<a href="/for-players/fixtures/" class="hidden-desktop">Latest Fixtures</a>
						<h2 class="hidden-phone hidden-tablet">Latest Fixtures</h2>
						<form id="teamselect" class="hidden-phone hidden-tablet">
							<fieldset>
								<select id="splash-division">
									<option selected="selected" value="4">Division 2</option>
									<option value="5">Division 3</option>
									<option value="6">Division 4</option>
									<option value="11">Victorian Championship Womens Division</option>
									<option value="8">Victorian Championship Mens Division</option>
								</select>
								<select id="splash-team">
									<?php 
										$division = $_POST['division'];
										$divisionSearch = $wpdb->get_results("SELECT * FROM wp_leaguemanager_teams WHERE league_id = 4 AND season = 'Summer 2016'");
										foreach($divisionSearch as $team){
											echo '<option value="'.$team->id.'">'.$team->title.'</option>';
										};
									?>
								</select>
								<button>View</button>
							</fieldset>
						</form>
					</div>
					<div id="ladders">
						<a href="/for-players/fixtures/" class="hidden-desktop">Latest Ladders</a>
						<h2 class="hidden-phone hidden-tablet">Latest Ladders</h2>
						<form id="ladderselect" class="hidden-phone hidden-tablet" action="e" method="get">
							<fieldset>
								<select id="ladder-division">
									<option selected="selected" value="4">Division 2</option>
									<option value="5">Division 3</option>
									<option value="6">Division 4</option>
									<option value="11">Victorian Championship Womens Division</option>
									<option value="8">Victorian Championship Mens Division</option>
								</select>
								<button>View</button>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!--//End Splash-->
	
	<!--//Social-->
	
	<div id="frame-social" class="hidden-phone">
		<div class="row-fluid">
			<div class="span8">
				<div class="left-container">
					<div class="internal">
						<div class="iconset icon-mail float-left"></div>
						<p class="float-left">For all enquiries, please feel free to <a href="mailto:admin@powerassist.org.au">e-mail us</a> or call us on (03) 9513 0304.</p>
						<div class="clear"></div>
					</div>
				</div>
			</div>
			<div class="span4">
			
			</div>
		</div>
	</div>
	
	<!--//End Content-->
	
	<!--//Content-->
	
	<div id="frame-content" class="hidden-phone">
		<div id="drag" class="row-fluid">
			<div class="span8">
				<div class="left-container">
					<div class="internal">
						<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
							<h1><?php the_title(); ?></h1>
							<?php the_content();  ?>
						<?php endwhile; ?>
					</div>
				</div>
			</div>
			<div class="span4">
				<div class="dragit"></div>
				<div id="gallery">
					<?php
						echo wp_get_attachment_image( get_field('image_1'), "homepageGALLERY" );
						echo wp_get_attachment_image( get_field('image_2'), "homepageGALLERY" );
						echo wp_get_attachment_image( get_field('image_3'), "homepageGALLERY" );
						echo wp_get_attachment_image( get_field('image_4'), "homepageGALLERY" );
						echo wp_get_attachment_image( get_field('image_5'), "homepageGALLERY" );
						echo wp_get_attachment_image( get_field('image_6'), "homepageGALLERY" );
					?>
				</div>
			</div>
		</div>
	</div>
	
	<!--//End Content-->
	
	<!--//Call to Action-->
	
	<div id="frame-cta">
		<div class="row-fluid">
			<div class="span8">
				<div class="left-container">
					<div class="internal">
						<?php 
							$sections = get_pages('include=14,13,19&sort_order=desc');
							foreach($sections as $post){
							setup_postdata($post);
						?>
								<div class="span4">
									<h3><?php echo $post->post_title ?></h3>
									<?php the_post_thumbnail('homepageCTA'); ?>
									<p><?php echo the_field('page_summary',$post->ID); ?></p>
									<a href="<?php echo get_permalink( $post->ID ); ?>">More</a>
								</div>
						<?php 
							}
							wp_reset_postdata();
						 ?>
					</div>
				</div>
			</div>
			<div class="span4 hidden-phone">
				<div class="right-container">
					<h3>The Stadiums</h3>
					<a id="close">Close</a>
					<dl>
						<dt>MLC<a href="https://maps.google.com.au/maps/ms?msa=0&msid=202274528488412393999.0004d967fe4afc16ec649&hl=en&ie=UTF8&t=m&ll=-37.800697,145.073605&spn=0.060359,0.171661&z=13&vpsrc=0&iwloc=0004d96801a9670b99e55&f=d&daddr=MLC+Basketball+Stadium+%40-37.814786,145.038512">directions</a></dt>
						<dd>207 Barkers Road. Kew, VIC.</dd>
						<dt>Hawthorn <a href="https://maps.google.com.au/maps/ms?msa=0&msid=202274528488412393999.0004d967fe4afc16ec649&hl=en&ie=UTF8&t=m&ll=-37.823752,145.073605&spn=0.06034,0.171661&z=13&vpsrc=0&iwloc=0004d967ff9408aa65918&f=d&daddr=Hawthorn+Basketball+Stadium+%40-37.83985,145.045971">directions</a></dt>
						<dd>Cnr Tooronga Rd &amp; Burgess St. Hawthorn East, VIC.</dd>
					</dl>
				</div>
				<iframe width="1000" height="445" frameborder="0" src="https://maps.google.com.au/maps/ms?msa=0&amp;msid=202274528488412393999.0004d967fe4afc16ec649&amp;hl=en&amp;ie=UTF8&amp;t=m&amp;ll=-37.825921,145.073605&amp;spn=0.052203,0.17149&amp;z=13&amp;output=embed"></iframe>
			</div>
		</div>
	</div>
	
	<!--//End Call to Action-->


<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-footer' ) ); ?>