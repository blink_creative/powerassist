$(document).ready(function() {
	$(function() {
		var Page = (function() {
			var $nav = $( '#nav-dots > span' ),
				slitslider = $( '#slider' ).slitslider( {
					onBeforeChange : function( slide, pos ) {
						$nav.removeClass( 'nav-dot-current' );
						$nav.eq( pos ).addClass( 'nav-dot-current' );
					}
				} ),
				init = function() {
					initEvents();
				},
				initEvents = function() {
					$nav.each( function( i ) {
						$( this ).on( 'click', function( event ) {
							var $dot = $( this );
							if( !slitslider.isActive() ) {
								$nav.removeClass( 'nav-dot-current' );
								$dot.addClass( 'nav-dot-current' );
							}
							slitslider.jump( i + 1 );
							return false;
						} );
					} );
				};
				return { init : init };
		})();
		Page.init();
	});
	
	/*--drag gallery--*/

	$(function() {
		var minX = -3420+1140; // farthest to left it can go
		var maxX = 0; // farthest to right it can go
		var yPosition = 0; // set to your y position
		$("#drag").draggable({
			containment: [minX,yPosition,maxX,yPosition],
			axis: "x",
			revert: false,
			helper: function(){
				// Create an invisible div as the helper. It will move and
				// follow the cursor as usual.
				return $('<div></div>').css('opacity',0);
			},
			create: function(){
				// When the draggable is created, save its starting
				// position into a data attribute, so we know where we
				// need to revert to.
				var $this = $(this);
				$this.data('starttop',$this.position().left);
			},
			drag: function(event, ui){
				// During dragging, animate the original object to
				// follow the invisible helper with custom easing.
				$(this).stop().animate({
					left: ui.helper.position().left
				},1000,'easeOutCirc');
			}
		});
	});
	
	/*--Close it--*/
	
	$('#close').click(function(){
		$('#frame-cta .right-container').fadeOut();
		$('#frame-content .right-container').fadeOut();
	});
	
	/*--Foldout--*/
	
	$(function() {
		var pfold = $( '#uc-container' ).pfold({
				easing : 'ease-in-out',
				folds : 3,
				speed: 300,
				folddirection : ['left','bottom','bottom']
			});
		$( '#frame-splash button' ).on( 'click', function() {
			pfold.unfold();
		} ).end().find( 'span.icon-cancel' ).on( 'click', function() {
			pfold.fold();
		} );
	});	
	
	$('#frame-splash button').click(function(e){
		  e.preventDefault();
	});
	
	
	/*--dragit--*/
	
	$('#frame-content').hover(function(){
		$('.dragit').stop(true, true).fadeOut();
	},
	function(){ 
		$('.dragit').stop(true, true).fadeIn();
	});
	
	/*--Team Finder--*/
	
	function requestDiv(){
		var division = $('#sel-division').val();
		var requestdivision = $.ajax({
			url: "http://power.blinkinteractive.com.au/wp-admin/admin-ajax.php",
			type: "POST",
			data: {
				action: 'divisionSelect',
				division: division
			},
			cache: false,
			success: function(html){
				$('#sel-team').html(html);
				$('#teamselect .none').fadeIn();
			}
		});
	};
	
	
	function selectTeam(){
		var division = $('#sel-division').val();
		var team = $('#sel-team').val();
		var requestteam = $.ajax({
			url: "http://power.blinkinteractive.com.au/wp-admin/admin-ajax.php",
			type: "POST",
			data: {
				action: 'teamSelect',
				division: division,
				team: team
			},
			cache: false,
			success: function(html){
				$('#overview-container').empty();
				$('#overview-container').append('<div class="bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div>');
				$('#overview-container').html(html);
				$('#overview-container').remove('div.bubblingG');
				$('#overview-container').fadeIn();
			}
		});
	};
	
	
	$('#sel-division').change(function(){
		requestDiv();
	});
	
	$('#sel-team').change(function(){
		selectTeam();
	});
	
	requestDiv();
	selectTeam();
	
	
	/*-- Initialize Teams --*/
	
	
	
	/*--Alert Box--*/
	
	$('#urgent-message').modal(
		{
			keyboard: true
		}
	);
	
	/*--Team Splash--*/
	
	function splashDivision(){
		var division = $('#splash-division').val();
		var requestdivision = $.ajax({
			url: "http://power.blinkinteractive.com.au/wp-admin/admin-ajax.php",
			type: "POST",
			data: {
				action: 'divisionSelect',
				division: division
			},
			cache: false,
			success: function(html){
				$('#splash-team').html(html);
			}
		});
	}
	
	function teamSelect(){
		$('.uc-final-content #overview').empty();
		var division = $('#splash-division').val();
		var team = $('#splash-team').val();
		var requestteam = $.ajax({
			url: "http://power.blinkinteractive.com.au/wp-admin/admin-ajax.php",
			type: "POST",
			data: {
				action: 'fixtureOverview',
				division: division,
				team: team
			},
			cache: false,
			success: function(html){
				$('.uc-final-content').append('<div class="bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div>');
				$('.uc-final-content').append(html);
				$('div.bubblingG').remove();
			}
		});
	}
	
	
	
	$('#splash-division').change(function(){
		splashDivision();
	});
	$('#teamselect button').click(function(){
		teamSelect();
	});
	$('#ladderselect button').click(function(){
		$('.uc-final-content #overview').empty();
		var division = $('#ladder-division').val();
		var requestteam = $.ajax({
			url: "http://power.blinkinteractive.com.au/wp-admin/admin-ajax.php",
			type: "POST",
			data: {
				action: 'ladderOverview',
				division: division
			},
			cache: false,
			success: function(html){
				$('.uc-final-content').append('<div class="bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div>');
				$('.uc-final-content').append(html);
				$('div.bubblingG').remove();
			}
		});
	});
	
	splashDivision();
	teamSelect();
	


});

