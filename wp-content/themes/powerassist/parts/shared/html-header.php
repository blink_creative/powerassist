<!DOCTYPE HTML>
	<!--[if IEMobile 7 ]><html class="no-js iem7" manifest="default.appcache?v=1"><![endif]--> 
	<!--[if lt IE 7 ]><html class="no-js ie6" lang="en"><![endif]--> 
	<!--[if IE 7 ]><html class="no-js ie7" lang="en"><![endif]--> 
	<!--[if IE 8 ]><html class="no-js ie8" lang="en"><![endif]--> 
	<!--[if (gte IE 9)|(gt IEMobile 7)|!(IEMobile)|!(IE)]><!--><html class="no-js" lang="en"><!--<![endif]-->
	<head>
		<title><?php wp_title( '|' ); ?></title>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0"><!-- Remove if you're not building a responsive site. (But then why would you do such a thing?) -->
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
		<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico"/>
							
		<!--//Styles -->
		
		<link rel="stylesheet" type="text/css" href="<?php bloginfo( 'template_url' ); ?>/css/style-min.css" />
		
		<!--//jQuery-->
		
		<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/lib/jquery-core.js"></script>

		<!--//Wordpress-->
		
		<?php wp_deregister_script('jquery');  ?>
		
	</head>
	<body <?php body_class(); ?>>
	
	<!--//Header-->

		<div id="frame-header">
			<div class="row-fluid">
				<div id="navigation-main" class="navbar">
					<div class="span8">
						<div class="left-container">
							<div class="internal">
							<a class="brand" href="/" title="Link to homepage"><img src="<?php bloginfo( 'template_url' ); ?>/images/logo-main.jpg" alt="Power Assist Logo"/></a></div>
						</div>
					</div>
					<button data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar" type="button">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<div class="span4">
						<div class="right-container">
							<div class="nav-collapse collapse">
								<?php wp_nav_menu('menu_class=nav&container=')?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	<!--//End Header-->
