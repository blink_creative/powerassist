	<!--//Links-->
	
	<div id="frame-links"  class="hidden-phone">
		<div class="row-fluid">
			<div class="span8">
				<div class="left-container">
					<div class="internal">
						<div class="span8">
							<h3>Recent Articles <a href="/news/">more</a></h3><?php rewind_posts(); ?>
							<dl> 
								<?php
									$recent = new WP_Query();
									$recent->query('showposts=3');
									if($recent->have_posts()) : while($recent->have_posts()): $recent->the_post();
								?>
									<dt><a href="<?php the_permalink();?>" title="<?php the_title();?>" ><?php the_title();?></a> </dt>
									<dd><?php the_excerpt();?></dd>
								<?php endwhile ?>
									<?php else : ?>
									<p> Nothing to see here. Move along </p>
								<?php endif ?>							
						</dl>
						</div>
						<div class="span4">
							<h3>Quick links</h3>
							<ul>
								<li><a href="/about-us/history/">Our History</a></li>
								<li><a href="/about-us/our-patron-andrew-gaze/">Our Patron</a></li>
								<li><a href="/about-us/our-achievements/">Our Achievements</a></li>
								<li><a href="/about-us/our-goals/">Our Goals</a></li>
								<li class="hidden-tablet"><a href="/about-us/what-we-do/">What We Do</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div id="partners" class="span4">
				<div class="right-container">
					<div class="title">
						<h3 class="float-left">Our Friends</h3>
						<div class="float-right small-button">
							<a href="/our-partners/">More</a>
						</div>
						<div class="clear"></div>
					</div>
					<ul>
						<li><a href="/our-partners/">Basketball Victoria</a></li>
						<li><a href="/our-partners/">Bendigo Bank</a></li>
						<li><a href="/our-partners/">Spartans</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	
	<!--//End Links-->
	
	<!--//Links-->
	
	<div id="frame-footer">
		<div class="row-fluid">
			<div class="span8 hidden-phone">
				<div class="left-container">
					<div class="internal">
						<p>&copy; Copyright <?php the_time('Y'); ?>, Power Assist. </p>
						<p><a href="http://www.blinkcreative.com.au">Digital Agency Melbourne</a> | Blink Creative.</p>
					</div>
				</div>
			</div>
			<div class="span4">
				<div class="right-container">
					<a href="/"><img src="<?php bloginfo( 'template_url' ); ?>/images/logo-footer.gif" alt="Power Assist Homepage"/></a>
				</div>
			</div>
		</div>
	</div>
	
	<!--//End Links-->
	
	<!--//Scripts-->
	
		<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/js/js-min.js"></script>
	
	<!--//End Scripts-->
	
	<!--//Analytics-->
	
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
		  ga('create', 'UA-46388401-1', 'powerassist.org.au');
		  ga('send', 'pageview');
		
		</script>	
		
	<!--//End Analytics-->
	
	<?php wp_footer(); ?>
	
	</body>
</html>