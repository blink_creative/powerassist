<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header' ) ); ?>


<div id="subpage">

	<!--//Splash-->
	
	<div id="frame-splash">
		<div class="row-fluid">
			<div class="span8">
				<div class="left-container">
					<h1><?php the_title(); ?></h1>
				</div>
			</div>
		</div>
	</div>
	
	<!--//End Splash-->
	
	<!--//Social-->
	
	<div id="frame-social" class="hidden-phone">
		<div class="row-fluid">
			<div class="span8">
				<div class="left-container">
					<div class="internal">
						<div class="iconset icon-mail float-left"></div>
						<p class="float-left">For all enquiries, please feel free to <a href="mailto:powerassist@optushome.com.au">e-mail us</a> or call us on (03) 9729 4155.</p>
					</div>
				</div>
			</div>
			<div class="span4">
			</div>
		</div>
	</div>
	
	<!--//End Content-->
	
	<!--//Content-->
	
	<div id="frame-content">
		<ul id="iphone-subnav" class="hidden-desktop hidden-tablet">
			<?php wp_list_pages('title_li='); ?>
		</ul>
		<div class="row-fluid">
			<div class="span8">
				<div class="left-container">
					<div class="internal">
						<h1><?php the_title(); ?></h1>
						<p>Page not found. Please use the navigation above to find out where you need to be!</p>
					</div>
				</div>
			</div>
			<div class="span4 hidden-phone">
				<div id="navigation-subpage">
					<h3>Get Involved</h3>
					<p>Want to join the club? Or maybe help volunteer?</p>
					<a class="more" href="#">More</a>
					<div class="clear"></div>
					<ul>
						<?php	
							$current = $post->ID;
							$parent = $post->post_parent;
							$grandparent_get = get_post($parent);
							$grandparent = $grandparent_get->post_parent; 
							$pagekids = get_pages("child_of=".$post->ID."&sort_column=menu_order");
							if ($pagekids) {
									if (wt_get_depth() == 1){wp_list_pages('depth=2&title_li=&child_of='.$parent.'');}
							} else {
									if (wt_get_depth() == 1){wp_list_pages('depth=2&title_li=&child_of='.$parent.'');}
							 }; 						
							 if (wt_get_depth() == 0){wp_list_pages('depth=1&title_li=&child_of='.$current.'');}
							 if (wt_get_depth() == 2){wp_list_pages('depth=2&title_li=&child_of='.$grandparent.'');}
						?>
					</ul>
				</div>
				<div id="map">
					<div class="right-container">
						<h3>The Stadiums</h3>
						<a id="close">Close</a>
						<dl>
							<dt>MLC<a href="https://maps.google.com.au/maps/ms?msa=0&msid=202274528488412393999.0004d967fe4afc16ec649&hl=en&ie=UTF8&t=m&ll=-37.800697,145.073605&spn=0.060359,0.171661&z=13&vpsrc=0&iwloc=0004d96801a9670b99e55&f=d&daddr=MLC+Basketball+Stadium+%40-37.814786,145.038512">directions</a></dt>
							<dd>207 Barkers Road. Kew, VIC.</dd>
							<dt>Hawthorn <a href="https://maps.google.com.au/maps/ms?msa=0&msid=202274528488412393999.0004d967fe4afc16ec649&hl=en&ie=UTF8&t=m&ll=-37.823752,145.073605&spn=0.06034,0.171661&z=13&vpsrc=0&iwloc=0004d967ff9408aa65918&f=d&daddr=Hawthorn+Basketball+Stadium+%40-37.83985,145.045971">directions</a></dt>
							<dd>Cnr Tooronga Rd &amp; Burgess St. Hawthorn East, VIC.</dd>
						</dl>
					</div>
					<iframe width="1000" height="445" src="https://maps.google.com.au/maps/ms?msa=0&amp;msid=202274528488412393999.0004d967fe4afc16ec649&amp;hl=en&amp;ie=UTF8&amp;t=m&amp;ll=-37.825921,145.073605&amp;spn=0.052203,0.17149&amp;z=13&amp;output=embed"></iframe>
				</div>
			</div>
		</div>
	</div>
	
	<!--//End Content-->
	
</div>


<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-footer' ) ); ?>
