<?php return BackWPup_Job::__set_state(array(
   'job' => 
  array (
    'type' => 
    array (
      0 => 'DBDUMP',
      1 => 'FILE',
      2 => 'WPPLUGIN',
    ),
    'destinations' => 
    array (
      0 => 'DROPBOX',
    ),
    'name' => 'Power Assist Backup',
    'activetype' => 'wpcron',
    'logfile' => '/home/powerass/public_html/wp-content/uploads/backwpup-6ecb21-logs/backwpup_log_6ecb21_2014-02-02_23-59-18.html',
    'lastbackupdownloadurl' => '',
    'cronselect' => 'basic',
    'cron' => '0 23 * * *',
    'mailaddresslog' => 'admin@igeekify.com',
    'mailaddresssenderlog' => 'BackWPup Power Assist <admin@igeekify.com>',
    'mailerroronly' => true,
    'backuptype' => 'archive',
    'archiveformat' => '.tar.gz',
    'archivename' => 'backwpup_%Y-%m-%d',
    'maxbackups' => 15,
    'backupdir' => '/home/powerass/public_html/wp-content/uploads/backwpup-6ecb21-backups/',
    'backupsyncnodelete' => true,
    'emailaddress' => 'breon@blinkcreative.com.au',
    'emailefilesize' => 20,
    'emailsndemail' => 'breon@blinkcreative.com.au',
    'emailsndemailname' => 'BackWPup Power Assist',
    'emailmethod' => '',
    'emailsendmail' => '/usr/sbin/sendmail -t -i',
    'emailhost' => 'www.powerassist.org.au',
    'emailhostport' => 25,
    'emailsecure' => '',
    'emailuser' => '',
    'emailpass' => '',
    'ftphost' => '',
    'ftphostport' => 21,
    'ftptimeout' => 90,
    'ftpuser' => '',
    'ftppass' => '',
    'ftpdir' => 'power-assist/',
    'ftpmaxbackups' => 15,
    'ftppasv' => true,
    'ftpssl' => false,
    'dropboxtoken' => 'fr7v41fmzokw80i7',
    'dropboxsecret' => '$BackWPup$RIJNDAEL$E5A07YFc+nlDooFYQ6gOHngnKbEX6xFTOb0XKC41Fq0=',
    'dropboxroot' => 'dropbox',
    'dropboxmaxbackups' => 15,
    'dropboxsyncnodelete' => false,
    'dropboxdir' => 'Power-Assist/',
    's3accesskey' => '',
    's3secretkey' => '',
    's3bucket' => '',
    's3region' => 'us-east-1',
    's3base_url' => '',
    's3ssencrypt' => '',
    's3storageclass' => '',
    's3dir' => 'Power-Assist/',
    's3maxbackups' => 15,
    's3syncnodelete' => true,
    's3multipart' => true,
    'msazureaccname' => '',
    'msazurekey' => '',
    'msazurecontainer' => '',
    'msazuredir' => 'Power-Assist/',
    'msazuremaxbackups' => 15,
    'msazuresyncnodelete' => true,
    'rscusername' => '',
    'rscapikey' => '',
    'rsccontainer' => '',
    'rscregion' => 'DFW',
    'rscdir' => 'Power-Assist/',
    'rscmaxbackups' => 15,
    'rscsyncnodelete' => true,
    'sugarrefreshtoken' => '',
    'sugarroot' => '',
    'sugardir' => 'Power-Assist/',
    'sugarmaxbackups' => 15,
    'dbdumpexclude' => 
    array (
    ),
    'dbdumpfile' => 'powerass_main',
    'dbdumptype' => 'sql',
    'dbdumpfilecompression' => '',
    'backupexcludethumbs' => false,
    'backupspecialfiles' => true,
    'backuproot' => true,
    'backupcontent' => true,
    'backupplugins' => true,
    'backupthemes' => true,
    'backupuploads' => true,
    'backuprootexcludedirs' => 
    array (
    ),
    'backupcontentexcludedirs' => 
    array (
      0 => 'cache',
      1 => 'upgrade',
      2 => 'w3tc',
    ),
    'backuppluginsexcludedirs' => 
    array (
      0 => 'backwpup',
      1 => 'backwpup-pro',
    ),
    'backupthemesexcludedirs' => 
    array (
    ),
    'backupuploadsexcludedirs' => 
    array (
    ),
    'fileexclude' => '.tmp,.svn,.git,desktop.ini,.DS_Store',
    'dirinclude' => '',
    'wpexportcontent' => 'all',
    'wpexportfilecompression' => '',
    'wpexportfile' => 'Power-Assist.wordpress.%Y-%m-%d',
    'pluginlistfilecompression' => '',
    'pluginlistfile' => 'Power-Assist.pluginlist.%Y-%m-%d',
    'dbcheckwponly' => true,
    'dbcheckrepair' => false,
    'jobid' => 1,
    'lastrun' => 1391385558,
    'lastruntime' => 7910,
  ),
   'start_time' => 1391468584,
   'logfile' => '/home/powerass/public_html/wp-content/uploads/backwpup-6ecb21-logs/backwpup_log_6ecb21_2014-02-03_23-03-04.html',
   'temp' => 
  array (
    'PHP' => 
    array (
      'INI' => 
      array (
        'ERROR_LOG' => 'error_log',
        'ERROR_REPORTING' => '4983',
        'LOG_ERRORS' => '1',
        'DISPLAY_ERRORS' => '1',
        'HTML_ERRORS' => '1',
        'REPORT_MEMLEAKS' => '1',
        'ZLIB_OUTPUT_COMPRESSION' => '',
        'IMPLICIT_FLUSH' => '',
      ),
      'ENV' => 
      array (
        'TEMPDIR' => false,
      ),
    ),
    'folders_to_backup' => 
    array (
    ),
  ),
   'backup_folder' => '/home/powerass/public_html/wp-content/uploads/backwpup-6ecb21-temp/',
   'backup_file' => 'backwpup_2014-02-03.tar.gz',
   'backup_filesize' => 0,
   'pid' => 655660,
   'timestamp_last_update' => 1391483538.5378229618072509765625,
   'timestamp_script_start' => 1391483536.2139780521392822265625,
   'warnings' => 9,
   'errors' => 0,
   'lastmsg' => '<samp>2. Trying to make a list of folders to back up&#160;&hellip;</samp>',
   'lasterrormsg' => '<samp style="background-color:#ffc000;color:#fff">WARNING: Job restarts due to inactivity for more than 5 minutes.</samp>',
   'steps_todo' => 
  array (
    0 => 'CREATE',
    1 => 'JOB_DBDUMP',
    2 => 'JOB_FILE',
    3 => 'JOB_WPPLUGIN',
    4 => 'CREATE_MANIFEST',
    5 => 'CREATE_ARCHIVE',
    6 => 'DEST_DROPBOX',
    7 => 'END',
  ),
   'steps_done' => 
  array (
    0 => 'CREATE',
  ),
   'steps_data' => 
  array (
    'CREATE' => 
    array (
      'CALLBACK' => '',
      'NAME' => 'Job Start',
      'STEP_TRY' => 0,
    ),
    'JOB_DBDUMP' => 
    array (
      'NAME' => 'Database backup',
      'STEP_TRY' => 3,
      'SAVE_STEP_TRY' => 0,
      'dbdumpfile' => 'powerass_main.sql',
      'is_head' => true,
      'tables' => 
      array (
        'wp_commentmeta' => 
        array (
          'records' => '0',
          'start' => 0,
          'length' => 100,
        ),
        'wp_comments' => 
        array (
          'records' => '0',
          'start' => 0,
          'length' => 100,
        ),
        'wp_frm_fields' => 
        array (
          'records' => '11',
          'start' => 100,
          'length' => 25000,
        ),
        'wp_frm_forms' => 
        array (
          'records' => '2',
          'start' => 100,
          'length' => 25000,
        ),
        'wp_frm_item_metas' => 
        array (
          'records' => '28',
          'start' => 100,
          'length' => 25000,
        ),
        'wp_frm_items' => 
        array (
          'records' => '7',
          'start' => 100,
          'length' => 25000,
        ),
        'wp_leaguemanager_leagues' => 
        array (
          'records' => '5',
          'start' => 100,
          'length' => 25000,
        ),
        'wp_leaguemanager_matches' => 
        array (
          'records' => '404',
          'start' => 25100,
          'length' => 25000,
        ),
        'wp_leaguemanager_stats' => 
        array (
          'records' => '0',
          'start' => 0,
          'length' => 100,
        ),
        'wp_leaguemanager_teams' => 
        array (
          'records' => '75',
          'start' => 100,
          'length' => 25000,
        ),
        'wp_links' => 
        array (
          'records' => '0',
          'start' => 0,
          'length' => 100,
        ),
        'wp_options' => 
        array (
          'records' => '167',
          'start' => 25100,
          'length' => 25000,
        ),
      ),
    ),
    'JOB_FILE' => 
    array (
      'NAME' => 'File backup',
      'STEP_TRY' => 2,
      'SAVE_STEP_TRY' => 0,
    ),
    'JOB_WPPLUGIN' => 
    array (
      'NAME' => 'Installed plugins list',
      'STEP_TRY' => 0,
      'SAVE_STEP_TRY' => 0,
    ),
    'CREATE_MANIFEST' => 
    array (
      'NAME' => 'Creates manifest file',
      'STEP_TRY' => 0,
      'SAVE_STEP_TRY' => 0,
    ),
    'CREATE_ARCHIVE' => 
    array (
      'NAME' => 'Creates archive',
      'STEP_TRY' => 0,
      'SAVE_STEP_TRY' => 0,
    ),
    'DEST_DROPBOX' => 
    array (
      'NAME' => 'Backup to Dropbox',
      'STEP_TRY' => 0,
      'SAVE_STEP_TRY' => 0,
    ),
    'END' => 
    array (
      'NAME' => 'End of Job',
      'STEP_TRY' => 0,
    ),
  ),
   'step_working' => 'JOB_FILE',
   'substeps_todo' => 7,
   'substeps_done' => 